export interface Certificate {
    commonName: string;
    issuerName: string;
    issuanceDate: string;
    expirationDate: string;
}
