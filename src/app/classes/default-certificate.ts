import { Certificate } from '@interface/certificate';

export class DefaultCertificate implements Certificate {
    commonName: '';
    issuerName: '';
    issuanceDate: '';
    expirationDate: '';
}
