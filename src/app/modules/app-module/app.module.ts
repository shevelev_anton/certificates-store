import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app-root/app.component';
import { WindowToken, windowProvider } from '@window/window-factory';
import { CertificatesModule } from '@certificates-module/certificates.module';


@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, AppRoutingModule, BrowserAnimationsModule, CertificatesModule],
    providers: [
        { provide: WindowToken, useFactory: windowProvider },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
