import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificatesRouterComponent } from './components/router/certificates-router.component';
import { CertificatesViewerComponent } from './components/viewer/certificates-viewer.component';
import { CertificatesRoutingModule } from './certificates-routing.module';
import { DragNDropComponent } from './components/drag-n-drop/drag-n-drop.component';
import { DragNDropDirective } from './directives/drag-n-drop.directive';
import { CertificateViewComponent } from './components/certificate-view/certificate-view.component';
import { CertificateListComponent } from './components/certificate-list/certificate-list.component';
import { MaterialModule } from '@material-module/material-module';

const EXPORTS = [];

@NgModule({
    declarations: [
        EXPORTS,
        CertificatesRouterComponent,
        CertificatesViewerComponent,
        DragNDropComponent,
        DragNDropDirective,
        CertificateViewComponent,
        CertificateListComponent,
    ],
    imports: [CommonModule, CertificatesRoutingModule, MaterialModule],
    exports: [EXPORTS],
})
export class CertificatesModule {}
