import { Injectable, Inject } from '@angular/core';
import { Certificate } from '@interface/certificate';
import { CERTIFICATES } from '@certificates-module/certificates-routes';
import { WindowToken } from '@window/window-factory';
import { GenericSubject } from 'src/app/classes/generic-subject';
import { DefaultCertificate } from 'src/app/classes/default-certificate';

@Injectable({
    providedIn: 'root',
})
export class CertificatesStoreService {
    private $certificates: Certificate[] = [];
    public certificates = new GenericSubject<Certificate[]>([null]);

    public selectedCertificate = new GenericSubject<Certificate>(null);
    public isAddNewCertificate = false;

    constructor(@Inject(WindowToken) private window: Window) {
        this.getCertificatesFromLocalStore();
    }

    saveCertificate(certificate: Certificate): void {
        this.$certificates.push(certificate);
        this.certificates.value = this.$certificates;

        this.window.localStorage.setItem(
            CERTIFICATES,
            JSON.stringify(this.$certificates)
        );
    }

    getCertificatesFromLocalStore() {
        const savedCertificates = this.window.localStorage.getItem(
            CERTIFICATES
        );
        this.$certificates = this.certificates.value = savedCertificates
            ? JSON.parse(savedCertificates)
            : [];
    }
    toogleCertificate(): void {
        this.isAddNewCertificate = !this.isAddNewCertificate;
    }
    selectCertificate(certificate: Certificate): void {
        this.selectedCertificate.value = certificate;
        this.isAddNewCertificate = false;
    }
}
