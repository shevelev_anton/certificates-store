import { TestBed } from '@angular/core/testing';

import { CertificatesStoreService } from './certificates-store.service';

describe('CertificatesStoreService', () => {
  let service: CertificatesStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CertificatesStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
