import { Component, OnInit } from '@angular/core';
import { CertificatesStoreService } from '@certificates-module/services/certificates-store.service';

@Component({
    selector: 'app-certificates-viewer',
    templateUrl: './certificates-viewer.component.html',
    styleUrls: ['./certificates-viewer.component.scss'],
})
export class CertificatesViewerComponent implements OnInit {

    constructor(public css: CertificatesStoreService) {}

    ngOnInit(): void {}
    addCertificate(): void {
        this.css.toogleCertificate();
    }
}
