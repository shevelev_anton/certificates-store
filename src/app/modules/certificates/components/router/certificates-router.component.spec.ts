import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificatesRouterComponent } from './certificates-router.component';

describe('CRouterComponent', () => {
  let component: CertificatesRouterComponent;
  let fixture: ComponentFixture<CertificatesRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificatesRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificatesRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
