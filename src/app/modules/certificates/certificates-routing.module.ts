import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VIEWER } from './certificates-routes';
import { CertificatesViewerComponent } from './components/viewer/certificates-viewer.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: VIEWER,
        pathMatch: 'full',
    },
    {
        path: VIEWER,
        component: CertificatesViewerComponent,

    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CertificatesRoutingModule {}
